package com.hw.droid.hwcatalog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;

import hwdroid.app.BaseFragmentActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.indicator.HWTabPageIndicator;
import hwdroid.widget.item.Item;
import hwdroid.widget.itemview.ItemView;

public class SelectListViewActivity extends BaseFragmentActivity {
    private static final String[] CONTENT = new String[] { "Radio", "Check", "RightImage", "Normal" };
    HWTabPageIndicator mIndicator;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setActivityContentView(R.layout.simple_tabs);
        showBackKey(true);
        
        FragmentPagerAdapter adapter = new SampleFragmentPagerAdapter(getSupportFragmentManager());

        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setOffscreenPageLimit(4);
        pager.setAdapter(adapter);

        mIndicator = new HWTabPageIndicator(this);
        
        mIndicator.setViewPager(pager);
        if(getActionBarView() != null) {
            getActionBarView().setTabPageIndicator(mIndicator);
        }
    }
    
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	ItemView view = (ItemView) v;
    	
    	ItemAdapter adapter = (ItemAdapter)l.getAdapter();
    	Item item = (Item)adapter.getItem(position);
    	item.setChecked(!item.isChecked());
    	view.setObject(item);
    }
    
    class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ListItemFragment.newInstance(CONTENT[position % CONTENT.length], position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }
    }
}
