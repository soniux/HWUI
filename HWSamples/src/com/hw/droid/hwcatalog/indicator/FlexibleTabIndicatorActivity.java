package com.hw.droid.hwcatalog.indicator;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.hw.droid.hwcatalog.ListItemFragment;
import com.hw.droid.hwcatalog.R;

import hwdroid.app.BaseFragmentActivity;
import hwdroid.widget.indicator.HWTabPageIndicator;


public class FlexibleTabIndicatorActivity extends BaseFragmentActivity {
    private static final String[] CONTENT = new String[] { "A", "B", "C", "D" };
    public static HWTabPageIndicator indicator;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setActivityContentView(R.layout.flexible_tabs);
        
        FragmentPagerAdapter adapter = new SampleFragmentPagerAdapter(this.getSupportFragmentManager());

        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);
        
        indicator = (HWTabPageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(pager);
    }

    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	ListItemFragment pager = ListItemFragment.newInstance(CONTENT[position % CONTENT.length], position);
        	return pager;
        }

        @SuppressLint("DefaultLocale")
		@Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }
    }
}
